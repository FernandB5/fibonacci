<?php
/**
 * File Name: ajax_fibonacci
 * Description: Do the Fibonacci alghorithm at server side
 * Version: 1.0
 * Author: Battisti Fernand
 * 
 */

function fibonacci($number)
{
    $arrayResults=array();
    $arrayResults["avant"]=$number;
    
    $fibonnaci_result = 1;
    $fibonnaci_actuel = 1;
    $fibonnaci_precedent = 0;
    if ($number > 0)
    {   
       $i = 0;
      
        while ($i < $number-2)
        { 
            $fibonnaci_precedent = $fibonnaci_actuel;
            $fibonnaci_actuel = $fibonnaci_result;
            $fibonnaci_result = $fibonnaci_actuel + $fibonnaci_precedent;
            $i++;
         }


        
    }
    $arrayResults["finale"]=$fibonnaci_result;

    return json_encode($arrayResults);
}

if(!empty($_POST['number']))
{
    $n=strip_tags(trim($_POST['number']));
    if(is_numeric($n))
    echo fibonacci($n);
    else
    echo "error";
}
else
{
    echo "error";
}
    
